export const state = () => ({
  chartLoading: false,
  optionLoading: false,
  strategyLoading: false,
  symbolsLoading: false,
  loadingCounter: 0
})
export const mutations = {
  loadingCounter (state) {
    state.loadingCounter++
  },
  chartLoading (state, chartLoading) {
    state.chartLoading = chartLoading
  },
  optionLoading (state, optionLoading) {
    state.optionLoading = optionLoading
  },
  strategyLoading (state, strategyLoading) {
    state.strategyLoading = strategyLoading
  },
  symbolsLoading (state, symbolsLoading) {
    state.symbolsLoading = symbolsLoading
  }
}
export const getters = {
  symbolsLoading: (state) => {
    return state.symbolsLoading
  },
  strategyLoading: (state) => {
    return state.strategyLoading
  },
  optionLoading: (state) => {
    return state.optionLoading
  },
  chartLoading: (state) => {
    return state.chartLoading
  },
  loadingCounter: (state) => {
    return state.loadingCounter
  }
}

import Vue from 'vue'
import vuex, { } from 'vuex'
import { jwtDecode } from 'jwt-decode'
const Cookies = require('js-cookie')
Vue.mixin({
  methods: {
    setJwtToCookie (jwt) {
      const payloadBase64 = jwt.split('.')[1]
      const payload = JSON.parse(atob(payloadBase64))
      this.$cookies.set('jwt', jwt, { expires: new Date(payload.exp * 1000) })
    },
    isJwtSet () {
      const jwt = this.$cookies.get('jwt')
      return !!jwt // بازگشت یک مقدار boolean بر اساس وجود یا عدم وجود JWT
    },
    jwt (jwt) {
      if (jwt !== undefined) {
        if (process.server) {
          this.$store.commit('jwt', jwt)
        } else {
          this.$store.commit('jwt', jwt)
          let jwtTTL = 0
          if (jwt.length > 0) {
            jwtTTL = this.parseJwt().exp - this.parseJwt().iat
          }
          Cookies.set('jwt', jwt, { expires: jwtTTL / 86400 })
        }
        this.$axios.defaults.headers.common.jwt = jwt
      } else if (process.server) {
        return this.$store.state.jwt
      } else {
        return Cookies.get('jwt')
      }
    },
    parseJwt () {
      const token = this.jwt()
      const tokens = token.replace('Bearer ', '')
      const decoded = jwtDecode(tokens)
      return decoded
    },
    getUser () {
      if (this.isObjectEmpty(this.parseJwt())) { return {} }
      return this.parseJwt().res
    },
    isObjectEmpty (obj) {
      if (!obj) { return true }
      // eslint-disable-next-line no-unreachable-loop
      for (const i in obj) {
        return false
      }
      return true
    }
  }
})
Vue.use(vuex)

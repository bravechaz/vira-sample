export default ({ $axios, app }) => {
  $axios.onRequest((config) => {
    config.headers.common.Authorization = app.$cookies.get('jwt')
  })
}

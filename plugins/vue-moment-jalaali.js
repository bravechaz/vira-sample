import Vue from 'vue'
import moment from 'moment-jalaali'
import 'moment/locale/fa'

moment.loadPersian({ dialect: 'persian-modern' })

Vue.use(require('vue-moment-jalaali'), { moment })

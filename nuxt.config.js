export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 3001,
    host: 'localhost'
  },
  head: {
    title: 'Vira project',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Vira Sample Project' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
            'https://cdn-stuff.s3.ir-thr-at1.arvanstorage.ir/bootstrap.min.css'
      },
      {
        rel: 'stylesheet',
        href:
            'https://cdn-stuff.s3.ir-thr-at1.arvanstorage.ir/video-js.min.css'
      }
    ],
    script: [
      {
        src: 'https://cdn-stuff.s3.ir-thr-at1.arvanstorage.ir/jquery-3.5.1.min.js',
        type: 'text/javascript'
      },
      {
        src:
            'https://cdn-stuff.s3.ir-thr-at1.arvanstorage.ir/popper.min.js',
        type: 'text/javascript'
      },
      {
        src:
            'https://cdn-stuff.s3.ir-thr-at1.arvanstorage.ir/bootstrap.min.js',
        type: 'text/javascript'
      },
      {
        src:
            'https://cdn-stuff.s3.ir-thr-at1.arvanstorage.ir/video.min.js'
      }
    ]
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@assets/styles.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@plugins/index.js',
    '@plugins/axios-config.js',
    '@plugins/vue-moment-jalaali.js'
  ],
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', {
      fix: true
    }],
    ['nuxt-animejs'],
    ['cookie-universal-nuxt', { alias: 'cookies' }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://vtiapi.tadbirkish.com/api/v1/'
  },
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
